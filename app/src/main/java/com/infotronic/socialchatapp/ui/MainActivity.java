package com.infotronic.socialchatapp.ui;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.fragment.chat.ChatFragment;
import com.infotronic.socialchatapp.ui.fragment.learning.LearningCatFragment;
import com.infotronic.socialchatapp.ui.fragment.login.SettingFragment;
import com.infotronic.socialchatapp.ui.fragment.moment.MomentFragment;

public class MainActivity extends ActivityBase {
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    DatabaseReference databaseReference;
    private FragmentManager mFragmentManager;
    BottomNavigationView bottomNavigationView;

    @Override
    public Context getActivityContext() {
        return MainActivity.this;
    }

    @Override
    public int getContentViewRid() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFragmentManager = getSupportFragmentManager();

        loadInitialFragment();

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        Fragment selectedFragment = null;

        switch (item.getItemId()) {
            case R.id.chat:
                Log.d("debug_paul", "home");
                selectedFragment = new ChatFragment();
                mFragmentManager.executePendingTransactions();
                mFragmentManager.beginTransaction().replace(R.id.container, selectedFragment, Config.CHATTING_FRAGMENT).commitAllowingStateLoss();
                break;
            case R.id.moment:
                Log.d("debug_paul", "moment");
                selectedFragment = new MomentFragment();
                mFragmentManager.executePendingTransactions();
                mFragmentManager.beginTransaction().replace(R.id.container, selectedFragment, Config.MOMENT_FRAGMENT).commitAllowingStateLoss();
                break;
            case R.id.learn:
                Log.d("debug_paul", "learning");
                selectedFragment = new LearningCatFragment();
                mFragmentManager.executePendingTransactions();
                mFragmentManager.beginTransaction().replace(R.id.container, selectedFragment, Config.LEARNING_CAT_FRAGMENT).commitAllowingStateLoss();
                break;
            case R.id.setting:
                Log.d("debug_paul", "setting");
                selectedFragment = new SettingFragment();
                mFragmentManager.executePendingTransactions();
                mFragmentManager.beginTransaction().replace(R.id.container, selectedFragment, Config.SETTING_FRAGMENT).commitAllowingStateLoss();
                break;
        }
        return true;
    };

    public void setNavBarVisiable(boolean visiable) {
        if (visiable) {
            bottomNavigationView.setVisibility(View.GONE);
        } else {
            bottomNavigationView.setVisibility(View.VISIBLE);
        }
    }

    private void loadInitialFragment() {
        Fragment initialFragment = ChatFragment.newInstance();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, initialFragment, Config.CHATTING_FRAGMENT);
        fragmentTransaction.commit();
    }

    public void logout() {
        FirebaseAuth.getInstance().signOut();
        Intent intentLogout = new Intent(getApplicationContext(), LaunchScreenActivity.class);
        startActivity(intentLogout);
        finish();
    }
}