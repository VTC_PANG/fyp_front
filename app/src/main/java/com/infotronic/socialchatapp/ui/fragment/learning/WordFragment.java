package com.infotronic.socialchatapp.ui.fragment.learning;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.roomDB.entity.Words;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.MainActivity;
import com.infotronic.socialchatapp.ui.ViewModel.WordsViewModel;
import com.infotronic.socialchatapp.ui.adapter.ChatAdapter;
import com.infotronic.socialchatapp.ui.fragment.chat.ChatRecordFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

//for template
public class WordFragment extends FragmentBase implements FragmentBackHandler {

    @BindView(R.id.learning_word)
    TextView learning_word;
    @BindView(R.id.count)
    TextView count;
    @BindView(R.id.timer)
    TextView timer;
    @BindView(R.id.tv_voice)
    TextView tv_voice;
    @BindView(R.id.voice)
    Button voice;
    @BindView(R.id.next)
    Button next;
    @BindView(R.id.btn_voice)

    ImageButton btn_voice;
    CoordinatorLayout chatAppbar;
    private Unbinder unbinder;

    private FragmentManager mFragmentManager;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    private WordsViewModel wordsViewModel;

    private static final String ARG_FILE_NAME = "ARG_FILE_NAME";
    static Random random = new Random();
    private String filename;
    private int wordlist_pos = 0;

    List<String> wordlist = new ArrayList<>();
    List<Words> AllWords;

    private ChatAdapter chatAdapter;
    MainActivity activity = null;
    TextToSpeech textToSpeech;
    CountDownTimer setTimer;
    HashMap<String, Object> hashMap = new HashMap<>();
    private static final int REQUEST_CODE_SPEECH_INPUT = 1000;

    public static WordFragment newInstance(String filename) {
        WordFragment wordFragment = new WordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FILE_NAME, filename);
        wordFragment.setArguments(args);
        return wordFragment;
    }

    public static WordFragment newInstance() {
        return new WordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            filename = getArguments().getString(ARG_FILE_NAME);
        }
        if (getActivity() instanceof MainActivity) {
            activity = (MainActivity) getActivity();
            textToSpeech = new TextToSpeech(activity.getActivityContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    int lang = textToSpeech.setLanguage(Locale.ENGLISH);
                }
            });
        }
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        wordsViewModel = ViewModelProviders.of(getActivity()).get(WordsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_learning_game, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        wordsViewModel.getAllWords().observe(getViewLifecycleOwner(), new Observer<List<Words>>() {
            @Override
            public void onChanged(List<Words> words) {
                AllWords = words;
                Log.d("debug_paul", "all words: " + AllWords.toString());
            }
        });
            String JsonObjWord = loadJSONFromAsset(activity.getActivityContext(), filename);

            for (int i = 0; i < 5; i++) {
                String word = getWord(JsonObjWord);
                wordlist.add(word.toLowerCase(Locale.ROOT));
            }
            learning_word.setText(wordlist.get(wordlist_pos));
            count.setText("1/5");

            if(setTimer != null){
                setTimer.cancel();
            }
            setCountDownTimer();

        voice.setOnClickListener(voiceOnClickListener);
        next.setOnClickListener(nextWordOnClickListener);
        btn_voice.setOnClickListener(RecordOnClickListener);


//        onRefreshView();
        return root;
    }

    private void setCountDownTimer(){
        setTimer = new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("" + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                timer.setText("0");
                timer.setTextColor(getResources().getColor(R.color.colorRed));
                next.setEnabled(true);
            }

        }.start();
    }

    private String getWord(String object){
        String word = null;
        JSONObject obj = null;
        JSONArray JsonArrWord = null;
        try {
            obj = new JSONObject(object);

            JsonArrWord = obj.getJSONArray("lists");
            int rand = random.nextInt(JsonArrWord.length());
            word = JsonArrWord.get(rand).toString();

            if (filename.equals("verbs")) {
                obj = new JSONObject(word);
                word = obj.get("present").toString();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return word;
    }

    private View.OnClickListener voiceOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String word = learning_word.getText().toString();
            int speech = textToSpeech.speak(word, TextToSpeech.QUEUE_FLUSH, null);
        }
    };

    private View.OnClickListener nextWordOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goNext();


        }
    };

    private View.OnClickListener RecordOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            speck();
        }
    };

    public String loadJSONFromAsset(Context context, String filename) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(filename+ ".json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_learning_game;
    }

    private ChatAdapter.onItemClickListner onItemClickListner = new ChatAdapter.onItemClickListner() {
        @Override
        public void onClick(int pos, String user_id) {
            Log.d("debug_test", "user_id: " + user_id);
            if (getActivity() instanceof MainActivity) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setNavBarVisiable(true);
            }
            gotoRecordFragment(user_id);
        }
    };

    private void saveLocalWord(){
        Words words = new Words();
        words.setWord(wordlist.get(wordlist_pos));
        words.setType(filename);
        int timeleft = Integer.parseInt(timer.getText().toString()) ;
        int score = 0;
        if(timeleft > 25 ){
            score = 10;
        } else if(timeleft < 25 && timeleft > 15){
            score = 7;
        } else if(timeleft < 15 && timeleft > 5){
            score = 5;
        }else{
            score = 2;
        }
        words.setScore(score);
        AllWords.add(words);
    }

    private void goNext(){
        saveLocalWord();

        if(wordlist_pos < 4) {
            wordlist_pos++;
            learning_word.setText(wordlist.get(wordlist_pos));
            learning_word.setTextColor(getResources().getColor(R.color.colorWhite));
            next.setEnabled(false);
            tv_voice.setText("");
            count.setText(wordlist_pos+1 + "/5");
            setCountDownTimer();
            timer.setTextColor(getResources().getColor(R.color.colorGreen));
            return;
        }
        wordlist_pos = 0;
        wordsService.insertAll(AllWords);
        learning_word.setText("Finnish");
        next.setVisibility(View.INVISIBLE);
        voice.setVisibility(View.INVISIBLE);
        timer.setVisibility(View.INVISIBLE);
        btn_voice.setVisibility(View.INVISIBLE);
        tv_voice.setVisibility(View.INVISIBLE);
    }

    private void speck(){
        final Intent speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en");
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try to repeat the word");

        try {
            startActivityForResult(speechRecognizerIntent, REQUEST_CODE_SPEECH_INPUT);
        } catch (Exception e){

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_CODE_SPEECH_INPUT:
                ArrayList<String> result = null;
                if (data != null) {
                    result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    tv_voice.setText(result != null ? result.get(0).toLowerCase(Locale.ROOT) : null);
                    boolean enable = clickable();
                    if(enable){
                        setTimer.cancel();
                    }
                    setWordColor(enable);
                    next.setEnabled(enable);

                    if(timer.getText().equals("0")){
                        next.setEnabled(true);
                    }

                }

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if(setTimer != null) {
            setTimer.cancel();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goBack();
        }
    };

    private Boolean clickable(){
        return tv_voice.getText().toString().equals(learning_word.getText().toString());
    }
    private void setWordColor(boolean result){
        if(result){
            learning_word.setTextColor(getResources().getColor(R.color.colorGreen));
        }else {
            learning_word.setTextColor(getResources().getColor(R.color.colorRed));
        }
    }

    private void goBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }

    private void gotoRecordFragment(String user_id) {
        Fragment fragment = ChatRecordFragment.newInstance(user_id);
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.CHATTING_FRAGMENT);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, Config.CHATTING_RECORD_FRAGMENT);
        fragmentTransaction.show(fragment);
        fragmentTransaction.hide(curFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onBackPressed() {
        //if you don't need take care on back button, remove implements FragmentBackHandler
        goBack();
        return false;
    }
}
