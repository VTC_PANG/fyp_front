package com.infotronic.socialchatapp.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.ViewModel.ChatRecordViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.ChatViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.TestViewModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter {
    Context context;
    List<ChatViewModel> chatViewModels;

    onItemClickListner onItemClickListner;

    public ChatAdapter(Context context) {
        this.context = context;
        chatViewModels = new ArrayList<>();
    }

    public void addModels(List<ChatViewModel> chatViewModels) {
        this.chatViewModels = chatViewModels;
        Log.d("debug_paul", "chatViewModels: " + chatViewModels.size());
        notifyDataSetChanged();
    }

    public void insertModels(ChatViewModel chatViewModels) {
        int pos = this.chatViewModels.size();
        this.chatViewModels.add(pos, chatViewModels);
        notifyItemInserted(pos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.card_chat_list, parent, false);
        return new ItemHolder(row);
    }
    public interface onItemClickListner{
        void onClick(int pos, String user_id);//pass your object types.
    }

    public void setOnItemClickListner(onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatViewModel mCurrentItem = chatViewModels.get(position);
        ItemHolder itemHolder = (ItemHolder) holder;
        itemHolder.profileImage.setTag(target);

        itemHolder.tvTradeCat.setText(mCurrentItem.user.getUsername());
        Picasso.get()
                .load(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .priority(Picasso.Priority.HIGH)
                .resize(30,30)
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .into((target = new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        itemHolder.profileImage.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        itemHolder.profileImage.setImageDrawable(errorDrawable);
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        itemHolder.profileImage.setImageDrawable(placeHolderDrawable);
                    }
                }));
        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Config.clickable) {
                    onItemClickListner.onClick(position, mCurrentItem.user.getId());
                }
            }
        });
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            // Bitmap is loaded, use image here
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    public int getItemCount() {
        return chatViewModels.size();
    }

    public class ItemHolder extends ViewHolder {
        @BindView(R.id.profile_image)
        CircleImageView profileImage;
        @BindView(R.id.tv_trade_cat)
        TextView tvTradeCat;
        @BindView(R.id.tv_trade_last_msg)
        TextView tvTradeLastMsg;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
