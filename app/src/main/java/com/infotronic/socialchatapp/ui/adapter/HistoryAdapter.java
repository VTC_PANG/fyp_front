package com.infotronic.socialchatapp.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.ViewModel.WordsHistoryViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.WordsHistoryViewModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HistoryAdapter extends RecyclerView.Adapter {
    Context context;
    List<WordsHistoryViewModel> wordsHistoryViewModels;

    onItemClickListner onItemClickListner;

    public HistoryAdapter(Context context) {
        this.context = context;
        wordsHistoryViewModels = new ArrayList<>();
    }

    public void addModels(List<WordsHistoryViewModel> wordsHistoryViewModels) {
        this.wordsHistoryViewModels = wordsHistoryViewModels;
        Log.d("debug_paul", "wordsHistoryViewModels: " + wordsHistoryViewModels.size());
        notifyDataSetChanged();
    }

    public void insertModels(WordsHistoryViewModel wordsHistoryViewModels) {
        int pos = this.wordsHistoryViewModels.size();
        this.wordsHistoryViewModels.add(pos, wordsHistoryViewModels);
        notifyItemInserted(pos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.card_history_list, parent, false);
        return new ItemHolder(row);
    }
    public interface onItemClickListner{
        void onClick(int pos, String words);//pass your object types.
    }

    public void setOnItemClickListner(onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WordsHistoryViewModel mCurrentItem = wordsHistoryViewModels.get(position);
        ItemHolder itemHolder = (ItemHolder) holder;

        itemHolder.tv_type.setText(mCurrentItem.words.getType());
        itemHolder.tv_word.setText(mCurrentItem.words.getWord());
        itemHolder.tv_score.setText(mCurrentItem.words.getScore() + "");

        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Config.clickable) {
                    onItemClickListner.onClick(position, mCurrentItem.words.getWord());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return wordsHistoryViewModels.size();
    }

    public class ItemHolder extends ViewHolder {
        @BindView(R.id.tv_trade_word_type)
        TextView tv_type;
        @BindView(R.id.tv_trade_word)
        TextView tv_word;
        @BindView(R.id.tv_trade_word_score)
        TextView tv_score;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
