package com.infotronic.socialchatapp.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.ViewModel.ChatRecordViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.ChatViewModel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatRecordAdapter extends RecyclerView.Adapter {
    Context context;
    List<ChatRecordViewModel> chatRecordViewModels;

    onItemClickListner onItemClickListner;

    public ChatRecordAdapter(Context context) {
        this.context = context;
        chatRecordViewModels = new ArrayList<>();
    }

    public void addModels(List<ChatRecordViewModel> chatRecordViewModels) {
        this.chatRecordViewModels = chatRecordViewModels;
        Log.d("debug_paul", "chatViewModels: " + chatRecordViewModels.size());
        notifyDataSetChanged();
    }
    public void clearModels() {
        this.chatRecordViewModels = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void insertModels(ChatRecordViewModel chatRecordViewModel) {
        int pos = this.chatRecordViewModels.size();
        this.chatRecordViewModels.add(pos, chatRecordViewModel);
        Log.d("debug_paul", "insertModels: " + chatRecordViewModels.size());
        notifyItemInserted(pos);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.card_message_item, parent, false);
        return new ItemHolder(row);
    }

    public interface onItemClickListner {
        void onClick(int pos, String description);//pass your object types.
    }

    public void setOnItemClickListner(onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatRecordViewModel mCurrentItem = chatRecordViewModels.get(position);
        ItemHolder itemHolder = (ItemHolder) holder;
        String ID;
        if(mCurrentItem.message.getSender().equals(Config.getUid())) ID = "SEND_ID"; else ID = "RECEIVE_ID";
        switch (ID){
            case "SEND_ID":
                itemHolder.tvMessage.setText(mCurrentItem.message.getMessages());
                itemHolder.tvMessage.setVisibility(View.VISIBLE);
                itemHolder.tvBotMessage.setVisibility(View.INVISIBLE);
                break;
            case "RECEIVE_ID":
                itemHolder.tvBotMessage.setText(mCurrentItem.message.getMessages());
                itemHolder.tvBotMessage.setVisibility(View.VISIBLE);
                itemHolder.tvMessage.setVisibility(View.INVISIBLE);
                break;
        }

        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Config.clickable) {
                    onItemClickListner.onClick(position, mCurrentItem.message.getMessages());
                }
            }
        });
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            // Bitmap is loaded, use image here
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    public int getItemCount() {
        return chatRecordViewModels.size();
    }

    public class ItemHolder extends ViewHolder {
        @BindView(R.id.tv_message)
        TextView tvMessage;
        @BindView(R.id.tv_bot_message)
        TextView tvBotMessage;
        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
