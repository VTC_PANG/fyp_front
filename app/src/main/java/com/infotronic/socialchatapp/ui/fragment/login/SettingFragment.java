package com.infotronic.socialchatapp.ui.fragment.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.MainActivity;
import com.infotronic.socialchatapp.ui.ViewModel.ChatViewModel;
import com.infotronic.socialchatapp.ui.adapter.ChatAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import butterknife.Unbinder;

//for template
public class SettingFragment extends FragmentBase implements FragmentBackHandler {

    @BindView(R.id.setting_toolbar_list)
    MaterialToolbar settingToolbarList;
    @BindView(R.id.setting_appbar)
    CoordinatorLayout settingAppbar;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.card_logout)
    MaterialCardView cardLogout;
    private Unbinder unbinder;

    private FragmentManager mFragmentManager;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    public static SettingFragment newInstance() {
        return new SettingFragment();
    }


    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

        onRefreshView();

        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_test;
    }

    private void onRefreshView() {
        settingToolbarList.setNavigationOnClickListener(navOnClickListener);
    }

    private ChatAdapter.onItemClickListner onItemClickListner = new ChatAdapter.onItemClickListner() {
        @Override
        public void onClick(int pos, String description) {
            Log.d("debug_test", "description: " + description);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goBack();
        }
    };

    private void goBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onBackPressed() {
        //if you don't need take care on back button, remove implements FragmentBackHandler
        goBack();
        return false;
    }

    @OnTouch(R.id.btn_logout)
    public void onViewClicked() {
        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.logout();
        }
    }
}
