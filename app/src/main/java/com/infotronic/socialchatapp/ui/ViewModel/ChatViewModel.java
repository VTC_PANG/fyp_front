package com.infotronic.socialchatapp.ui.ViewModel;

import androidx.lifecycle.ViewModel;

import com.infotronic.socialchatapp.roomDB.entity.Users;

public class ChatViewModel extends ViewModel {
    public Users user;
    public String msg;

    public ChatViewModel() {}

    public ChatViewModel(Users user) {
        this.user = user;
    }
    public ChatViewModel(Users user, String msg) {
        this.user = user;
        this.msg = msg;
    }

}