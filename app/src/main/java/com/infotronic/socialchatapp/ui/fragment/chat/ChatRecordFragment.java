package com.infotronic.socialchatapp.ui.fragment.chat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.help.appbase.BotResponse;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.help.appbase.Time;
import com.infotronic.socialchatapp.roomDB.entity.Message;
import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.LaunchScreenActivity;
import com.infotronic.socialchatapp.ui.MainActivity;
import com.infotronic.socialchatapp.ui.ViewModel.ChatRecordViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.ChatViewModel;
import com.infotronic.socialchatapp.ui.adapter.ChatRecordAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Semaphore;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

//for template
public class ChatRecordFragment extends FragmentBase implements FragmentBackHandler {

    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.tv_trade_cat)
    TextView tvTradeCat;
    @BindView(R.id.chat_record_toolbar_list)
    Toolbar chatRecordToolbarList;
    @BindView(R.id.chat_record_appbar)
    AppBarLayout chatRecordAppbar;
    @BindView(R.id.recyclerView_address)
    RecyclerView recyclerViewAddress;
    @BindView(R.id.et_message)
    EditText etMessage;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.ll_layout_bar)
    LinearLayout llLayoutBar;
    private Unbinder unbinder;

    private FragmentManager mFragmentManager;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    private static final String ARG_USER_ID = "ARG_USER_ID";
    private String fuser_id;

    List<Message> messagesList = new ArrayList<>();
    List<ChatRecordViewModel> chatRecordViewModels = new ArrayList<>();

    private ChatRecordAdapter chatRecordAdapter;


    public static ChatRecordFragment newInstance() {
        return new ChatRecordFragment();
    }

    public static ChatRecordFragment newInstance(String fuser_id) {
        ChatRecordFragment chatRecordFragment = new ChatRecordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_ID, fuser_id);
        chatRecordFragment.setArguments(args);
        return chatRecordFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fuser_id = getArguments().getString(ARG_USER_ID);
        }
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_chat_record, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

        onRefreshView();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//        databaseReference = FirebaseDatabase.getInstance().getReference("user").child(firebaseUser.getUid());
        Log.d("debug_paul", firebaseUser.getUid());

        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_chat_record;
    }

    private void onRefreshView() {
        chatRecordAdapter = new ChatRecordAdapter(getContext());
        recyclerViewAddress.setLayoutManager(new LinearLayoutManager(getContext()));
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference docRef = db.collection("users").document(fuser_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Users users = document.toObject(Users.class);
                    ((AppCompatActivity) getActivity()).setSupportActionBar(chatRecordToolbarList);
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(users.getUsername());
                    profileImage.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    chatRecordToolbarList.setNavigationOnClickListener(navOnClickListener);
                } else {
                    Log.d("debug_paul", "get failed with ", task.getException());
                }
            }
        });
        messagesList = new ArrayList<>();
        chatRecordViewModels = new ArrayList<>();
//        initDataList();
        readMessages();

        chatRecordAdapter.setOnItemClickListner(onItemClickListner);
        recyclerViewAddress.setAdapter(chatRecordAdapter);

    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goBack();
        }
    };

    private ChatRecordAdapter.onItemClickListner onItemClickListner = new ChatRecordAdapter.onItemClickListner() {
        @Override
        public void onClick(int pos, String description) {
            Log.d("debug_test", "description: " + description);
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void goBack() {
        chatRecordAdapter.clearModels();
        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            activity.setNavBarVisiable(false);
        }
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.CHATTING_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.CHATTING_RECORD_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }

    public void BotResponse(Message messages) {
        String response = BotResponse.basicResponse(messages.getMessages());
//        sendMsg(response, fuser_id, Config.getUid());

        if(!response.equals("")) {
//            if(response.contains("www")) {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(response)));
//            }
            chatRecordAdapter.insertModels(new ChatRecordViewModel(new Message(Config.AI_ID, Config.getUid(), response, Time.timeStamp())));
//            if(chatRecordAdapter.getItemCount() != 1)
//            recyclerViewAddress.scrollToPosition(chatRecordAdapter.getItemCount() - 1);
        }

    }

    @Override
    public boolean onBackPressed() {
        //if you don't need take care on back button, remove implements FragmentBackHandler
        goBack();
        return false;
    }

    private void readMessages(){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("chats")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("debug_paul", "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            Message message = dc.getDocument().toObject(Message.class);
                            switch (dc.getType()) {
                                case ADDED:
                                    Log.d("debug_paul", "New Message: " + dc.getDocument().getData().get("receiver"));

                                    chatRecordAdapter.insertModels(new ChatRecordViewModel(message));
                                    Log.d("debug_paul", "chatRecordAdapter.getItemCount(): " + chatRecordAdapter.getItemCount());
//                                    if(chatRecordAdapter.getItemCount() != 1 || chatRecordAdapter.getItemCount() != 0)
//                                            recyclerViewAddress.smoothScrollToPosition(chatRecordAdapter.getItemCount());

                                    if(fuser_id.equals(Config.AI_ID) && !dc.getDocument().getData().get("sender").equals(fuser_id)) {
                                        BotResponse(message);
                                    }
                                    break;
                                case MODIFIED:
                                    Log.d("debug_paul", "Modified Message: " + message);
                                    break;
                                case REMOVED:
                                    Log.d("debug_paul", "Removed Message: " + dc.getDocument().getData());
                                    break;
                            }
                        }

                    }
                });

        db.collection("chats")
        .get()
        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Message message = document.toObject(Message.class);
                        Log.d("debug_paul", message.getTime());
                            if (
                                (message.getSender().equals(Config.getUid()) && message.getReceiver().equals(fuser_id)) ||
                                (message.getReceiver().equals(Config.getUid()) && message.getSender().equals(fuser_id)))
                            {
                                messagesList.add(message);
//                                chatRecordAdapter.insertModels(new ChatRecordViewModel(message));
                        }
                    }
                    loadList();

                } else {
                    Log.w("debug_paul", "Error getting documents.", task.getException());
                }
            }
        });
    }



    @OnClick(R.id.btn_send)
    public void onViewClicked() {
        String messages_body = etMessage.getText().toString();
        sendMsg(messages_body, Config.getUid(), fuser_id);
//        String time = Time.timeStamp();
//
//        final FirebaseFirestore db = FirebaseFirestore.getInstance();
//
//        HashMap<String, Object> hashMap = new HashMap<>();
//        hashMap.put("messages", messages_body);
//        hashMap.put("sender", Config.getUid());
//        hashMap.put("receiver", fuser_id);
//        hashMap.put("time", time);
//
        if(!messages_body.equals("")) {
            etMessage.setText("");
//            chatRecordAdapter.insertModels(new ChatRecordViewModel(messages));
//            recyclerViewAddress.scrollToPosition(chatRecordAdapter.getItemCount() - 1);
        }
//
//        db.collection("chats")
//                .document()
//                .set(hashMap)
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.w("debug_paul", "Error adding document", e);
//                    }
//                });
    }

    public void sendMsg(String msg, String send, String receiver){
        String time = Time.timeStamp();

        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("messages", msg);
        hashMap.put("sender", send);
        hashMap.put("receiver", receiver);
        hashMap.put("time", time);

        if(!msg.equals("")) {
            etMessage.setText("");
//            chatRecordAdapter.insertModels(new ChatRecordViewModel(messages));
//            recyclerViewAddress.scrollToPosition(chatRecordAdapter.getItemCount() - 1);
        }

        db.collection("chats")
                .document()
                .set(hashMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("debug_paul", "Error adding document", e);
                    }
                });
    }

    public void loadList(){
        Map<Date ,Message> hashMap = new TreeMap<Date, Message>();
        DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        List<Message> messages = new ArrayList<>();

        for (int i = 0; i < messagesList.size(); i++) {
            try {
                Date  date = f.parse(messagesList.get(i).getTime());
//                hashMap.put(f.parse(messagesList.get(i).getTime(),"");
                hashMap.put(date, messagesList.get(i));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        hashMap.forEach((key, value) -> {
//            System.out.print("hashMap: "+key + "=" + value + " ");
            messages.add(value);
        });

        for (int i = 0; i < messages.size(); i++) {
            chatRecordViewModels.add(new ChatRecordViewModel(messages.get(i)));
        }
        chatRecordAdapter.addModels(chatRecordViewModels);


    }
}
