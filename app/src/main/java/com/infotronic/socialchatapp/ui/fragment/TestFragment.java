package com.infotronic.socialchatapp.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.adapter.TestAdapter;
import com.infotronic.socialchatapp.ui.ViewModel.TestViewModel;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.ui.FragmentBase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

//for template
public class TestFragment extends FragmentBase implements FragmentBackHandler {
    @BindView(R.id.test_toolbar_list)
    MaterialToolbar testToolbarList;
    @BindView(R.id.test_recyclerView)
    RecyclerView testRecyclerView;
    @BindView(R.id.review_appbar)
    CoordinatorLayout reviewAppbar;
    private Unbinder unbinder;

    private FragmentManager mFragmentManager;

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    private TestAdapter testAdapter;


    public static TestFragment newInstance() {
        return new TestFragment();
    }


    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_test, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

//        onRefreshView();

        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_test;
    }

    private void onRefreshView() {
        testAdapter = new TestAdapter(getContext());
        testToolbarList.setNavigationOnClickListener(navOnClickListener);
        initDataList();
        testAdapter.setOnItemClickListner(onItemClickListner);
    }

    private TestAdapter.onItemClickListner onItemClickListner = new TestAdapter.onItemClickListner(){
        @Override
        public void onClick(int pos, String description) {
            Log.d("debug_test", "description: " + description);
        }
    };

    private void initDataList() {
        List<String> description = new ArrayList<>();
        for(int i = 10; description.size() < i; i++){
            description.add("Test " + i);
        }
        List<TestViewModel> testViewModels = new ArrayList<>();
        for (int i = 0; i < description.size(); i++) {
            testViewModels.add(new TestViewModel(description.get(i)));
        }
        testAdapter.addModels(testViewModels);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goBack();
        }
    };

    private void goBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onBackPressed() {
        //if you don't need take care on back button, remove implements FragmentBackHandler
        goBack();
        return false;
    }
}
