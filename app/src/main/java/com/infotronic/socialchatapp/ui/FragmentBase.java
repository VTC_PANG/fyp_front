package com.infotronic.socialchatapp.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;


import com.infotronic.socialchatapp.service.impl.UsersServiceImpl;
import com.infotronic.socialchatapp.service.impl.WordsServiceImpl;

import org.jetbrains.annotations.NotNull;

public abstract class FragmentBase extends Fragment {
    View view;
    private Dialog dialog;

    //===============db caller=======================================
    public static WordsServiceImpl wordsService = null;
//    public static FoodlibsServiceImpl foodlibsService = null;
//    public static FoodServiceImpl foodService = null;

    //===============================================================
    //fragment set up
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanseState) {
        initDbService(getContext());
        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.dismissDialog();
        }
        view = fragmentView(inflater, container, savedInstanseState);
        return view;
    }

    public void initDbService(Context context){
        if (wordsService == null)
            wordsService = new WordsServiceImpl(context);
//        if (userService == null)
//            userService = new UsersServiceImpl(context);
//        if (foodlibsService == null)
//            foodlibsService = new FoodlibsServiceImpl(context);
//        if (foodService == null)
//            foodService = new FoodServiceImpl(context);
    }

    public void resetDbService(){
//        userService = null;
//        foodlibsService = null;
//        foodService = null;
        initDbService(getContext());
    }

    public abstract View fragmentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    public abstract int getFragmentId();


}
