package com.infotronic.socialchatapp.ui.ViewModel;

import androidx.lifecycle.ViewModel;

import com.infotronic.socialchatapp.roomDB.entity.Articles;
import com.infotronic.socialchatapp.roomDB.entity.Users;

import java.util.List;

public class MomentViewModel extends ViewModel {
    public Users user;
    public Articles articles;

    public MomentViewModel() {}

    public MomentViewModel(Users user) {
        this.user = user;
    }

    public MomentViewModel(Users user, Articles articles) {
        this.user = user;
        this.articles = articles;
    }
}