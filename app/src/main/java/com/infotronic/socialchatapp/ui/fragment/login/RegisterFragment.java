package com.infotronic.socialchatapp.ui.fragment.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.LaunchScreenActivity;
import com.infotronic.socialchatapp.ui.MainActivity;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;
import butterknife.Unbinder;

public class RegisterFragment extends FragmentBase {

    @BindView(R.id.reg_tv_user_name)
    TextView regTvUserName;
    @BindView(R.id.reg_tv_user_name_error)
    TextView regTvUserNameError;
    @BindView(R.id.reg_tf_user_name)
    EditText regTfUserName;
    @BindView(R.id.reg_tv_email)
    TextView regTvEmail;
    @BindView(R.id.reg_tv_email_error)
    TextView regTvEmailError;
    @BindView(R.id.reg_tf_email)
    EditText regTfEmail;
    @BindView(R.id.reg_tv_pwd)
    TextView regTvPwd;
    @BindView(R.id.reg_tv_pwd_error)
    TextView regTvPwdError;
    @BindView(R.id.reg_tf_pwd)
    EditText regTfPwd;
    @BindView(R.id.reg_tv_confirm_pwd)
    TextView regTvConfirmPwd;
    @BindView(R.id.reg_tv_confirm_pwd_error)
    TextView regTvConfirmPwdError;
    @BindView(R.id.reg_tf_confirm_pwd)
    EditText regTfConfirmPwd;
    @BindView(R.id.register)
    Button register;
    private FragmentManager mFragmentManager;
    private DatabaseReference reference;
    FirebaseAuth auth;
    private Unbinder unbinder;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        auth = FirebaseAuth.getInstance();
        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_login;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnTouch(R.id.register)
    public void onViewClicked() {
        final String email, pwd, username;
        email = regTfEmail.getText().toString().trim();
        pwd = regTfPwd.getText().toString().trim();
        username = regTfUserName.getText().toString().trim();
        auth.createUserWithEmailAndPassword(email, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            //        auth.createUserWithEmailAndPassword("paulmingtest@gmail.com", "password").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser firebaseUser = auth.getCurrentUser();
                    final FirebaseFirestore db = FirebaseFirestore.getInstance();
                    final String user_id = firebaseUser.getUid();

                    reference = FirebaseDatabase.getInstance().getReference("users").child(user_id);

                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("id", user_id);
                    hashMap.put("username", username);
                    hashMap.put("imageURL", "default");
                    hashMap.put("email", email);

                    db.collection("users")
                            .document(user_id)
                            .set(hashMap)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    DocumentReference docRef = db.collection("users").document(user_id);
                                    docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                if (getActivity() instanceof LaunchScreenActivity) {
                                                    LaunchScreenActivity launchScreenActivity = (LaunchScreenActivity) getActivity();
                                                    launchScreenActivity.goToMain();
                                                }
                                            } else {
                                                Log.d("debug_paul", "get failed with ", task.getException());
                                            }
                                        }
                                    });
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w("debug_paul", "Error adding document", e);
                                }
                            });


                } else {
                }
            }
        });
//        auth.createUserWithEmailAndPassword("paulming0612@gmail.com", "password");
    }
}
