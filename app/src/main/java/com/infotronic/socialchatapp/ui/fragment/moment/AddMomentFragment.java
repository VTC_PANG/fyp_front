package com.infotronic.socialchatapp.ui.fragment.moment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.help.appbase.Time;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.MainActivity;
import com.infotronic.socialchatapp.ui.adapter.MomentAdapter;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

//for template
public class AddMomentFragment extends FragmentBase implements FragmentBackHandler {


    @BindView(R.id.moment_toolbar_list)
    MaterialToolbar momentToolbarList;
    @BindView(R.id.moment_appbar)
    CoordinatorLayout momentAppbar;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.ll_layout_bar)
    LinearLayout llLayoutBar;
    @BindView(R.id.tv_content)
    EditText tvContent;
    private Unbinder unbinder;

    private FragmentManager mFragmentManager;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;


    private MomentAdapter momentAdapter;


    public static AddMomentFragment newInstance() {
        return new AddMomentFragment();
    }


    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_add_moment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

        onRefreshView();

        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_moment;
    }

    private void onRefreshView() {
        momentAdapter = new MomentAdapter(getContext());

        momentToolbarList.setNavigationOnClickListener(navOnClickListener);

        momentAdapter.setOnItemClickListner(onItemClickListner);

    }

    private MomentAdapter.onItemClickListner onItemClickListner = new MomentAdapter.onItemClickListner() {
        @Override
        public void onClick(int pos, String description) {
            Log.d("debug_test", "description: " + description);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getActivity() instanceof MainActivity) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setNavBarVisiable(false);
            }
            goBack();
        }
    };

    private void goBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.MOMENT_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.ADD_MOMENT_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }

    public void sendArticles(String send) {
        String time = Time.timeStamp();

        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        HashMap<String, Object> hashMap = new HashMap<>();
        String content = tvContent.getText().toString().trim();
        hashMap.put("content", content);
        hashMap.put("sender", send);
        hashMap.put("time", time);

        if (!content.equals("")) {
            goBack();
//            chatRecordAdapter.insertModels(new ChatRecordViewModel(messages));
//            recyclerViewAddress.scrollToPosition(chatRecordAdapter.getItemCount() - 1);
        }

        db.collection("articles")
                .document()
                .set(hashMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("debug_paul", "Error adding document", e);
                    }
                });
    }

    @Override
    public boolean onBackPressed() {
        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            activity.setNavBarVisiable(false);
        }
        goBack();
        return false;
    }

    @OnClick(R.id.btn_send)
    public void onViewClicked() {
        sendArticles(Config.getUid());
    }
}
