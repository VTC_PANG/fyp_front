package com.infotronic.socialchatapp.ui.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.roomDB.entity.Words;
import com.infotronic.socialchatapp.service.db.WordsService;
import com.infotronic.socialchatapp.service.impl.WordsServiceImpl;

import java.util.List;

public class WordsHistoryViewModel extends ViewModel {
    public Words words;

    public WordsHistoryViewModel() {}

    public WordsHistoryViewModel(Words words) {
        this.words = words;
    }
}