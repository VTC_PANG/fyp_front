package com.infotronic.socialchatapp.ui.fragment.learning;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.roomDB.entity.Words;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.MainActivity;
import com.infotronic.socialchatapp.ui.ViewModel.TestViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.WordsHistoryViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.WordsViewModel;
import com.infotronic.socialchatapp.ui.adapter.HistoryAdapter;
import com.infotronic.socialchatapp.ui.adapter.TestAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

//for template
public class HistoryFragment extends FragmentBase implements FragmentBackHandler {
    @BindView(R.id.history_toolbar_list)
    MaterialToolbar historyToolbarList;
    @BindView(R.id.history_recyclerView)
    RecyclerView historyRecyclerView;
    private Unbinder unbinder;

    TextToSpeech textToSpeech;
    MainActivity activity = null;

    private FragmentManager mFragmentManager;

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;
    private WordsViewModel wordsViewModel;

    private HistoryAdapter historyAdapter;

    List<Words> AllWords;


    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }


    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        wordsViewModel = ViewModelProviders.of(getActivity()).get(WordsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_history, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        AllWords = wordsService.getAll();
        wordsViewModel.getAllWords().observe(getViewLifecycleOwner(), new Observer<List<Words>>() {
            @Override
            public void onChanged(List<Words> words) {
                AllWords = words;
                Log.d("debug_paul", "History "+AllWords.toString());
            }
        });
        if (getActivity() instanceof MainActivity) {
            activity = (MainActivity) getActivity();
            textToSpeech = new TextToSpeech(activity.getActivityContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    int lang = textToSpeech.setLanguage(Locale.ENGLISH);
                }
            });
        }

        onRefreshView();


        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_history;
    }

    private void onRefreshView() {
        historyAdapter = new HistoryAdapter(getContext());
        historyAdapter.setOnItemClickListner(onItemClickListner);

        initDataList();
        historyRecyclerView.setAdapter(historyAdapter);

        historyToolbarList.setNavigationOnClickListener(navOnClickListener);

    }

    private HistoryAdapter.onItemClickListner onItemClickListner = new HistoryAdapter.onItemClickListner(){
        @Override
        public void onClick(int pos, String description) {
            Log.d("debug_test", "word: " + description);
            int speech = textToSpeech.speak(description, TextToSpeech.QUEUE_FLUSH, null);
        }
    };

    private void initDataList() {
        List<WordsHistoryViewModel> wordsHistoryViewModels = new ArrayList<>();
        for (Words word: AllWords){
            wordsHistoryViewModels.add(new WordsHistoryViewModel(word));
        }

        historyAdapter.addModels(wordsHistoryViewModels);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goBack();
        }
    };


    private void goBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onBackPressed() {
        //if you don't need take care on back button, remove implements FragmentBackHandler
        goBack();
        return false;
    }
}
