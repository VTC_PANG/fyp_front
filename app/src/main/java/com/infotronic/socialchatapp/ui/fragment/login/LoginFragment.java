package com.infotronic.socialchatapp.ui.fragment.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.LaunchScreenActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.OnTouch;
import butterknife.Unbinder;

public class LoginFragment extends FragmentBase {
    private Unbinder unbinder;

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.Login_TextView_1)
    TextView LoginTextView1;
    @BindView(R.id.Login_TextView_1_error)
    TextView LoginTextView1Error;
    @BindView(R.id.tf_userName)
    EditText tfUserName;
    @BindView(R.id.Login_TextView_2)
    TextView LoginTextView2;
    @BindView(R.id.Login_TextView_2_error)
    TextView LoginTextView2Error;
    @BindView(R.id.tf_pwd)
    EditText tfPwd;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_register)
    Button btnRegister;
    private String mEmail;
    private String mPassword;

    private FragmentManager mFragmentManager;

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser != null){
            if (getActivity() instanceof LaunchScreenActivity) {
                LaunchScreenActivity launchScreenActivity = (LaunchScreenActivity) getActivity();
                launchScreenActivity.goToMain();
            }
        }

    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_login;
    }


    @OnTextChanged(value = {R.id.tf_userName, R.id.tf_pwd}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onChange() {
        mEmail = tfUserName.getText().toString();
        mPassword = tfPwd.getText().toString();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void login(){
        if (getActivity() instanceof LaunchScreenActivity) {
            LaunchScreenActivity launchScreenActivity = (LaunchScreenActivity) getActivity();
            mEmail = tfUserName.getText().toString().trim();
            mPassword = tfPwd.getText().toString().trim();

            if (mEmail.matches("")) {
                LoginTextView1Error.setVisibility(View.VISIBLE);
            } else {
                LoginTextView1Error.setVisibility(View.GONE);
            }

            if (mPassword.matches("")) {
                LoginTextView2Error.setVisibility(View.VISIBLE);
            } else {
                LoginTextView2Error.setVisibility(View.GONE);
            }

            if (!mEmail.matches("") && !mPassword.matches("")) {

                launchScreenActivity.login(mEmail, mPassword);
            }
        }
    }

    @OnTouch({R.id.btn_login, R.id.btn_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                login();
                break;
            case R.id.btn_register:
                Fragment nextFragment = RegisterFragment.newInstance();
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.login_fragment_container, nextFragment);
                fragmentTransaction.commit();
                break;
        }
    }
}
