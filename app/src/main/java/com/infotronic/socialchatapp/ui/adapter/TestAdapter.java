package com.infotronic.socialchatapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;


import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.ViewModel.TestViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestAdapter extends RecyclerView.Adapter {
    Context context;
    List<TestViewModel> testViewModels;

    onItemClickListner onItemClickListner;

    public TestAdapter(Context context) {
        this.context = context;
        testViewModels = new ArrayList<>();
    }

    public void addModels(List<TestViewModel> testViewModels) {
        this.testViewModels = testViewModels;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.card_test, parent, false);
        return new ItemHolder(row);
    }
    public interface onItemClickListner{
        void onClick(int pos, String description);//pass your object types.
    }

    public void setOnItemClickListner(onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final TestViewModel mCurrentItem = testViewModels.get(position);
        ItemHolder itemHolder = (ItemHolder) holder;
        itemHolder.tv_trade_cat.setText(mCurrentItem.description);
        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Config.clickable) {
                    onItemClickListner.onClick(position, mCurrentItem.description);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return testViewModels.size();
    }

    public class ItemHolder extends ViewHolder {
        @BindView(R.id.tv_trade_cat) TextView tv_trade_cat;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
