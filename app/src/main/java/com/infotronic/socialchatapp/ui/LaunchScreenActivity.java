package com.infotronic.socialchatapp.ui;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.Fade;
import androidx.transition.TransitionInflater;
import androidx.transition.TransitionSet;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.roomDB.AppDatabase;
import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.ui.fragment.login.LaunchScreenFragment;
import com.infotronic.socialchatapp.ui.fragment.login.LoginFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class LaunchScreenActivity extends ActivityBase {
    private static final long MOVE_DEFAULT_TIME = 1000;
    private static final long FADE_DEFAULT_TIME = 300;


    private FragmentManager mFragmentManager;

    private String[] permissionList = {
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };

    private Timer timer = new Timer();

    private FirebaseAuth auth;

    @Override
    public Context getActivityContext() {
        return LaunchScreenActivity.this;
    }

    @Override
    public int getContentViewRid() {
        return R.layout.activity_lanuch_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        mFragmentManager = getSupportFragmentManager();
        Log.d("debug_paul", "onCreate");
        loadInitialFragment();
        grantPermission();

    }


    final Thread setupThread = new Thread() {
        @Override
        public void run() {
            try {
                super.run();
                if (onLoad()) {
                    Log.d("debug_paul", "onLoad");
                    performTransition();
                } else {
                    throw new Exception("Missing File");
                }
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage());
            }
        }
    };


    private void loadInitialFragment() {
        Fragment initialFragment = LaunchScreenFragment.newInstance();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, initialFragment);
        fragmentTransaction.commit();
    }

    private void onback() {
//        Fragment initialFragment = LoginFragment.newInstance();
//        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.login_fragment_container, initialFragment);
//        fragmentTransaction.commit();
    }

    private void performTransition() {
        // more on this later
        if (isDestroyed()) {
            return;
        }
        Fragment previousFragment = mFragmentManager.findFragmentById(R.id.login_fragment_container);
        Fragment nextFragment = new LoginFragment();

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        // 1. Exit for Previous Fragment
        Fade exitFade = new Fade();
        exitFade.setDuration(FADE_DEFAULT_TIME);
        previousFragment.setExitTransition(exitFade);

        // 2. Shared Elements Transition
        TransitionSet enterTransitionSet = new TransitionSet();
        enterTransitionSet.addTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.move));
        enterTransitionSet.setDuration(MOVE_DEFAULT_TIME);
        enterTransitionSet.setStartDelay(FADE_DEFAULT_TIME);
        nextFragment.setSharedElementEnterTransition(enterTransitionSet);

        // 3. Enter Transition for New Fragment
        Fade enterFade = new Fade();
        enterFade.setStartDelay(MOVE_DEFAULT_TIME + FADE_DEFAULT_TIME);
        enterFade.setDuration(FADE_DEFAULT_TIME);
        nextFragment.setEnterTransition(enterFade);

        View logo = findViewById(R.id.logo);

        fragmentTransaction.addSharedElement(logo, logo.getTransitionName());
        fragmentTransaction.replace(R.id.login_fragment_container, nextFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected boolean onLoad() {
        SystemClock.sleep(800);
        return true;
    }


    private void grantPermission() {
        List<String> permissionListToBeGrant = new ArrayList<String>();
        for (String permission : permissionList) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), permission)
                    != PackageManager.PERMISSION_GRANTED) {
                permissionListToBeGrant.add(permission);
            }
        }

        if (permissionListToBeGrant.size() > 0) {
            String[] permissionListToBeGrantArray = new String[permissionListToBeGrant.size()];
            permissionListToBeGrantArray = permissionListToBeGrant.toArray(permissionListToBeGrantArray);
            Log.d(getLocalClassName(), "permissionListToBeGrantArray: " + permissionListToBeGrantArray.length);
            ActivityCompat.requestPermissions(this, permissionListToBeGrantArray, 1);
        } else {
            setupThread.start();
        }
    }

    private void fail() {
        new MaterialAlertDialogBuilder(LaunchScreenActivity.this)
                .setMessage(getResources().getString(R.string.emptyColError))
                .setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                })
                .show();
    }

//    public void getAccessToken(final String uname, final String pwd,
//                               final boolean ischecked) {
//        showDialog("Login...");
//        Map map = new HashMap();
//        map.put("username", uname);
//        map.put("password", pwd);
//        JSONObject jsonObject = new JSONObject(map);
//        AndroidNetworking.post(Config.BASE_URL + Config.API_TOKEN)
//                .addHeaders(NetworkHeaderHelper.getDefaultHeader())
//                .addJSONObjectBody(jsonObject)
//                .setPriority(Priority.HIGH)
//                .build()
//                .getAsOkHttpResponseAndObject(ResponseResult.class, new OkHttpResponseAndParsedRequestListener<ResponseResult>() {
//                    @Override
//                    public void onResponse(Response okHttpResponse, ResponseResult response) {
//                        Log.d("debug_paul", "token response:" + response.getResult());
//                        if ("success".equals(response.getStatus()) && "200".equals(response.getCode())) {
//                            Config.setTokenAccess(response.getResult().getToken());
//                            Config.setUname(uname);
//                            Config.setPwd(pwd);
//                            SharedPreferences settings = getSharedPreferences(Config.PREF_KEY, MODE_PRIVATE);
//                            SharedPreferences.Editor editor = settings.edit();
//                            Log.d("debug_paul", response.getResult().getToken());
//                            if (ischecked) {
//                                editor.putString(Config.PREF_Login_KEY, Base64Util.getEncodeStr(uname, pwd));
//                            }
//                            editor.apply();
//
////                            goToMain();
//                            getUserInfo();
//                        }
//
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
//                        dismissDialog();
//                        fail();
//                    }
//                });
//    }

    public void goToMain() {
        dismissDialog();
        Intent intent = new Intent(LaunchScreenActivity.this, MainActivity.class);
        startActivity(intent);
    }


    public void login(String email, final String password) {
        Log.d("LoginActivity", "login");
        auth = FirebaseAuth.getInstance();
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    AppDatabase.createDB(getActivityContext());
                    resetDbService();
                    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

                    final FirebaseFirestore db = FirebaseFirestore.getInstance();
                    String user_id = firebaseUser.getUid();
                    Config.setUid(user_id);
                    DocumentReference docRef = db.collection("users").document(user_id);
                    docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                Users users = document.toObject(Users.class);
                                if (document.exists()) {
                                    userService.insertAll(users);
                                    Log.d("debug_paul", "get db user: " + userService.getById(Config.getUid()));

                                }
                            } else {
                                Log.d("debug_paul", "get failed with ", task.getException());
                            }
                        }
                    });

                    goToMain();
                }else {
                    Log.w("debug_paul", task.getException());
                    Toast.makeText(LaunchScreenActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        getAccessToken(username, password, ischecked);
    }


    //hide soft keyboard when tap outside the editText
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            View view = this.getCurrentFocus();
            view.clearFocus();
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                grantPermission();
                break;
        }
    }
}
