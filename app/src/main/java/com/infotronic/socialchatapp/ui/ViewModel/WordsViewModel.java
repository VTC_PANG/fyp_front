package com.infotronic.socialchatapp.ui.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.infotronic.socialchatapp.roomDB.entity.Words;
import com.infotronic.socialchatapp.service.db.WordsService;
import com.infotronic.socialchatapp.service.impl.WordsServiceImpl;

import java.util.List;

public class WordsViewModel extends AndroidViewModel {
    public LiveData<List<Words>> AllWords;
    private WordsService wordsService;


    public WordsViewModel(@NonNull Application application) {
        super(application);
        wordsService = new WordsServiceImpl(application);
        AllWords = wordsService.getAllWords();
    }

    public LiveData<List<Words>> getAllWords() {
        return AllWords;
    }

}