package com.infotronic.socialchatapp.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.ui.ViewModel.MomentViewModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MomentAdapter extends RecyclerView.Adapter {
    Context context;
    List<MomentViewModel> momentViewModels;

    onItemClickListner onItemClickListner;



    public MomentAdapter(Context context) {
        this.context = context;
        momentViewModels = new ArrayList<>();
    }

    public void addModels(List<MomentViewModel> momentViewModels) {
        this.momentViewModels = momentViewModels;
        notifyDataSetChanged();
    }
    public void insertModels(MomentViewModel momentViewModels) {
        int pos = this.momentViewModels.size();
        this.momentViewModels.add(pos, momentViewModels);
        notifyItemInserted(pos);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.card_moment_body, parent, false);
        return new ItemHolder(row);
    }

    public interface onItemClickListner {
        void onClick(int pos, String description);//pass your object types.
    }

    public void setOnItemClickListner(onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MomentViewModel mCurrentItem = momentViewModels.get(position);
        ItemHolder itemHolder = (ItemHolder) holder;
        itemHolder.profileImage.setTag(target);

        itemHolder.tvTradeCat.setText(mCurrentItem.user.getUsername());
        itemHolder.content.setText(mCurrentItem.articles.getContent());
        Picasso.get()
                .load(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .priority(Picasso.Priority.HIGH)
                .resize(30, 30)
                .memoryPolicy(MemoryPolicy.NO_STORE, MemoryPolicy.NO_CACHE)
                .into((target = new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        itemHolder.profileImage.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                        itemHolder.profileImage.setImageDrawable(errorDrawable);
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        itemHolder.profileImage.setImageDrawable(placeHolderDrawable);
                    }
                }));

        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Config.clickable) {
                    onItemClickListner.onClick(position, mCurrentItem.user.getUsername());
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return momentViewModels.size();
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            // Bitmap is loaded, use image here
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    public class ItemHolder extends ViewHolder {
        @BindView(R.id.profile_image)
        CircleImageView profileImage;
        @BindView(R.id.tv_trade_cat)
        TextView tvTradeCat;
        @BindView(R.id.content)
        TextView content;
        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
