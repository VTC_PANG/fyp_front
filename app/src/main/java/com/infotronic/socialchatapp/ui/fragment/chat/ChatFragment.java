package com.infotronic.socialchatapp.ui.fragment.chat;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.roomDB.entity.Message;
import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.ui.MainActivity;
import com.infotronic.socialchatapp.ui.ViewModel.ChatRecordViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.ChatViewModel;
import com.infotronic.socialchatapp.ui.adapter.ChatAdapter;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.ui.FragmentBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

//for template
public class ChatFragment extends FragmentBase implements FragmentBackHandler {
    CoordinatorLayout reviewAppbar;
    @BindView(R.id.chat_toolbar_list)
    MaterialToolbar chatToolbarList;
    @BindView(R.id.test_recyclerView)
    RecyclerView testRecyclerView;
    @BindView(R.id.chat_appbar)
    CoordinatorLayout chatAppbar;
    private Unbinder unbinder;

    private FragmentManager mFragmentManager;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;


    private ChatAdapter chatAdapter;

    HashMap<String, Object> hashMap = new HashMap<>();

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }


    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_chat, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

        onRefreshView();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//        databaseReference = FirebaseDatabase.getInstance().getReference("user").child(firebaseUser.getUid());
        Log.d("debug_paul", firebaseUser.getUid());
        String user_id = firebaseUser.getUid();
        Config.setUid(user_id);
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference docRef = db.collection("users").document(user_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Users users = document.toObject(Users.class);
                    if (document.exists()) {
                        Log.d("debug_paul", "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d("debug_paul", "No such document");
                    }
                } else {
                    Log.d("debug_paul", "get failed with ", task.getException());
                }
            }
        });
        return root;
    }

    public void findUserById(String user_id){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference docRef = db.collection("users").document(user_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Users users = document.toObject(Users.class);
                    if (document.exists()) {
                        Log.d("debug_paul", "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d("debug_paul", "No such document");
                    }
                } else {
                    Log.d("debug_paul", "get failed with ", task.getException());
                }
            }
        });
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_chat;
    }

    private void onRefreshView() {
        chatAdapter = new ChatAdapter(getContext());
        testRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        chatToolbarList.setNavigationOnClickListener(navOnClickListener);
        initDataList();
        chatAdapter.setOnItemClickListner(onItemClickListner);
        testRecyclerView.setAdapter(chatAdapter);

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("chats")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("debug_paul", "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:
                                    Log.d("debug_paul", "New city: " + dc.getDocument().getData());
                                    Log.d("debug_paul", "hashMap.size: "+ hashMap.size());

                                    hashMap.forEach((key, value) -> {
                                        Log.d("debug_paul", "hashMap: "+key + "=" + value + " ");
                                        if(key.equals(dc.getDocument().getData().get("sender"))){
                                            hashMap.put(key, dc.getDocument().getData().get("messages"));
                                        }
                                    });
                                    hashMap.forEach((key, value) -> {
                                        Log.d("debug_paul", "after hashMap: "+key + "=" + value + " ");
                                    });
                                    break;
                                case MODIFIED:
                                    Log.d("debug_paul", "Modified city: " + dc.getDocument().getData());
                                    break;
                                case REMOVED:
                                    Log.d("debug_paul", "Removed city: " + dc.getDocument().getData());
                                    break;
                            }
                        }

                    }
                });

    }

    private ChatAdapter.onItemClickListner onItemClickListner = new ChatAdapter.onItemClickListner() {
        @Override
        public void onClick(int pos, String user_id) {
            Log.d("debug_test", "user_id: " + user_id);
            if (getActivity() instanceof MainActivity) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setNavBarVisiable(true);
            }
            gotoRecordFragment(user_id);
        }
    };

//    private void readMessages() {
//        final FirebaseFirestore db = FirebaseFirestore.getInstance();
//        db.collection("chats")
//                .addSnapshotListener(new EventListener<QuerySnapshot>() {
//                    @Override
//                    public void onEvent(@Nullable QuerySnapshot snapshots,
//                                        @Nullable FirebaseFirestoreException e) {
//                        if (e != null) {
//                            Log.w("debug_paul", "listen:error", e);
//                            return;
//                        }
//
//                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
//                            Message message = dc.getDocument().toObject(Message.class);
//                            switch (dc.getType()) {
//                                case ADDED:
//                                    Log.d("debug_paul", "New Message: " + dc.getDocument().getData().get("receiver"));
//                                    chatAdapter.insertModels(new ChatViewModel(message, dc.getDocument().getData().get("receiver").toString()));
//                                    Log.d("debug_paul", "chatRecordAdapter.getItemCount(): " + chatRecordAdapter.getItemCount());
//
//                                    break;
//                                case MODIFIED:
//                                    Log.d("debug_paul", "Modified Message: " + message);
//                                    break;
//                                case REMOVED:
//                                    Log.d("debug_paul", "Removed Message: " + dc.getDocument().getData());
//                                    break;
//                            }
//                        }
//
//                    }
//                });
//
//        db.collection("chats")
//                .get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
//
//                        if (task.isSuccessful()) {
//                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                Message message = document.toObject(Message.class);
//                                Log.d("debug_paul", message.getTime());
//                                if (
//                                        (message.getSender().equals(Config.getUid()) && message.getReceiver().equals(fuser_id)) ||
//                                                (message.getReceiver().equals(Config.getUid()) && message.getSender().equals(fuser_id))) {
//                                    messagesList.add(message);
////                                chatRecordAdapter.insertModels(new ChatRecordViewModel(message));
//                                }
//                            }
//                            loadList();
//
//                        } else {
//                            Log.w("debug_paul", "Error getting documents.", task.getException());
//                        }
//                    }
//                });
//    }

    private void initDataList() {
        List<Users> users = new ArrayList<>();
        hashMap = new HashMap<>();

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Users user = document.toObject(Users.class);
                                if (!user.getId().equals(firebaseUser.getUid())) {
                                    users.add(user);
                                }
                                Log.d("debug_paul", document.getId() + " => " + document.getData());
                                hashMap.put(user.getId(), "");
                                List<ChatViewModel> chatViewModels = new ArrayList<>();
                                for (int i = 0; i < users.size(); i++) {
                                    chatViewModels.add(new ChatViewModel(users.get(i)));
                                }

                                chatAdapter.addModels(chatViewModels);
                            }
                        } else {
                            Log.w("debug_paul", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goBack();
        }
    };

    private void goBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.DUMMY_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }

    private void gotoRecordFragment(String user_id) {
        Fragment fragment = ChatRecordFragment.newInstance(user_id);
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.CHATTING_FRAGMENT);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, Config.CHATTING_RECORD_FRAGMENT);
        fragmentTransaction.show(fragment);
        fragmentTransaction.hide(curFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onBackPressed() {
        //if you don't need take care on back button, remove implements FragmentBackHandler
        goBack();
        return false;
    }
}
