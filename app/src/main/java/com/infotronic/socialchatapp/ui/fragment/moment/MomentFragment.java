package com.infotronic.socialchatapp.ui.fragment.moment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.roomDB.entity.Articles;
import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.MainActivity;
import com.infotronic.socialchatapp.ui.ViewModel.MomentViewModel;
import com.infotronic.socialchatapp.ui.adapter.MomentAdapter;
import com.infotronic.socialchatapp.ui.fragment.chat.ChatRecordFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

//for template
public class MomentFragment extends FragmentBase implements FragmentBackHandler {

    @BindView(R.id.moment_toolbar_list)
    MaterialToolbar momentToolbarList;
    @BindView(R.id.moment_recyclerView)
    RecyclerView momentRecyclerView;
    @BindView(R.id.moment_appbar)
    CoordinatorLayout momentAppbar;
    @BindView(R.id.floating_action_button)
    FloatingActionButton floatingActionButton;
    private Unbinder unbinder;

    private FragmentManager mFragmentManager;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;


    private MomentAdapter momentAdapter;


    public static MomentFragment newInstance() {
        return new MomentFragment();
    }


    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_moment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();

        onRefreshView();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//        databaseReference = FirebaseDatabase.getInstance().getReference("user").child(firebaseUser.getUid());
//        databaseReference = FirebaseDatabase.getInstance().getReference("user").child(firebaseUser.getUid());
        Log.d("debug_paul", firebaseUser.getUid());
        String user_id = firebaseUser.getUid();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference docRef = db.collection("users").document(user_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("debug_paul", "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d("debug_paul", "No such document");
                    }
                } else {
                    Log.d("debug_paul", "get failed with ", task.getException());
                }
            }
        });
        checkMomentUpdate();
        return root;
    }

    @Override
    public int getFragmentId() {
        return R.layout.fragment_moment;
    }

    private void onRefreshView() {
        momentAdapter = new MomentAdapter(getContext());
        momentRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        momentToolbarList.setNavigationOnClickListener(navOnClickListener);
//        initDataList();

        momentAdapter.setOnItemClickListner(onItemClickListner);
        momentRecyclerView.setAdapter(momentAdapter);

    }

    private MomentAdapter.onItemClickListner onItemClickListner = new MomentAdapter.onItemClickListner() {
        @Override
        public void onClick(int pos, String description) {
            Log.d("debug_test", "description: " + description);
        }
    };

    private void initDataList() {
        List<Users> users = new ArrayList<>();
        List<Articles> articleslist = new ArrayList<>();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("articles")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Articles articles = document.toObject(Articles.class);

                                Log.d("debug_paul", document.getId() + " => " + document.getData());
                                findUserById(articles);

                            }
                        } else {
                            Log.w("debug_paul", "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    public void findUserById(Articles articles){

        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference docRef = db.collection("users").document(articles.getSender());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Users user = document.toObject(Users.class);

                    momentAdapter.insertModels(new MomentViewModel(user, articles));

                } else {
                    Log.d("debug_paul", "get failed with ", task.getException());
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private View.OnClickListener navOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goBack();
        }
    };

    private void goBack() {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.HOME_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.MOMENT_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }

    public void checkMomentUpdate(){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("articles")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("debug_paul", "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            DocumentSnapshot document = dc.getDocument();
                            Articles articles = document.toObject(Articles.class);
                            switch (dc.getType()) {
                                case ADDED:
                                    Log.d("debug_paul", "New Moment: " + dc.getDocument().getData());
                                    findUserById(articles);
                                    break;
                                case MODIFIED:
                                    Log.d("debug_paul", "Modified Moment: " + dc.getDocument().getData());
                                    break;
                                case REMOVED:
                                    Log.d("debug_paul", "Removed Moment: " + dc.getDocument().getData());
                                    break;
                            }
                        }

                    }
                });

    }

    @Override
    public boolean onBackPressed() {
        //if you don't need take care on back button, remove implements FragmentBackHandler
        goBack();
        return false;
    }

    private void gotoAddMomentFragment() {
        Fragment fragment = AddMomentFragment.newInstance();
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.MOMENT_FRAGMENT);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, Config.ADD_MOMENT_FRAGMENT);
        fragmentTransaction.show(fragment);
        fragmentTransaction.hide(curFragment);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.floating_action_button)
    public void onViewClicked() {
        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            activity.setNavBarVisiable(true);
        }
        gotoAddMomentFragment();
    }
}
