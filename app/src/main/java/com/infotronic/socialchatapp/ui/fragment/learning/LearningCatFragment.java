package com.infotronic.socialchatapp.ui.fragment.learning;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.R;
import com.infotronic.socialchatapp.help.appbase.FragmentBackHandler;
import com.infotronic.socialchatapp.roomDB.entity.Message;
import com.infotronic.socialchatapp.roomDB.entity.Words;
import com.infotronic.socialchatapp.ui.FragmentBase;
import com.infotronic.socialchatapp.ui.MainActivity;
import com.infotronic.socialchatapp.ui.ViewModel.ChatRecordViewModel;
import com.infotronic.socialchatapp.ui.ViewModel.WordsViewModel;
import com.infotronic.socialchatapp.ui.adapter.ChatRecordAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

//for template
public class LearningCatFragment extends FragmentBase implements FragmentBackHandler {
    @BindView(R.id.learning_toolbar_list)
    MaterialToolbar toolbar_list;

    @BindView(R.id.card_noun)
    MaterialCardView cardNoun;
    @BindView(R.id.card_verb)
    MaterialCardView cardVerb;
    @BindView(R.id.card_adjective)
    MaterialCardView cardAdj;
    @BindView(R.id.card_adverb)
    MaterialCardView cardAdverb;

    @BindView(R.id.tv_num_of_noun)
    TextView tvNumOfNoun;
    @BindView(R.id.tv_num_of_verb)
    TextView tvNumOfVerb;
    @BindView(R.id.tv_num_of_adjective)
    TextView tvNumOfAdj;
    @BindView(R.id.tv_num_of_adverb)
    TextView tvNumOfAdverb;

    private Unbinder unbinder;

    private FragmentManager mFragmentManager;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    private static final String ARG_USER_ID = "ARG_USER_ID";
    private String fuser_id;
    private WordsViewModel wordsViewModel;
    List<Words> AllWords;

    List<Message> messagesList = new ArrayList<>();
    List<ChatRecordViewModel> chatRecordViewModels = new ArrayList<>();

    private ChatRecordAdapter chatRecordAdapter;


    public static LearningCatFragment newInstance() {
        return new LearningCatFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fuser_id = getArguments().getString(ARG_USER_ID);
        }
    }

    public View fragmentView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        wordsViewModel = ViewModelProviders.of(getActivity()).get(WordsViewModel.class);

        View root = inflater.inflate(R.layout.fragment_learning_cat_list, container, false);
        unbinder = ButterKnife.bind(this, root);
        mFragmentManager = getActivity().getSupportFragmentManager();
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar_list);

        wordsViewModel.getAllWords().observe(getViewLifecycleOwner(), new Observer<List<Words>>() {
            @Override
            public void onChanged(List<Words> words) {
                AllWords = words;
                int numOfNouns = 0;
                int numOfAdjs = 0;
                int numOfAdverbs = 0;
                int numOfVerbs = 0;

                for (Words word: AllWords) {
                    switch (word.getType()){
                        case "nouns":
                            numOfNouns ++;
                            tvNumOfNoun.setText(numOfNouns+"");
                            break;
                        case "adjs":
                            numOfAdjs ++;
                            tvNumOfAdj.setText(numOfAdjs+"");
                            break;
                        case "adverbs":
                            numOfAdverbs ++;
                            tvNumOfAdverb.setText(numOfAdverbs+"");
                            break;
                        case "verbs":
                            numOfVerbs ++;
                            tvNumOfVerb.setText(numOfVerbs+"");
                            break;
                    }

                }
            }
        });
//        databaseReference = FirebaseDatabase.getInstance().getReference("user").child(firebaseUser.getUid());

        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.history_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.history:
                goToHistory();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

        @Override
    public int getFragmentId() {
        return R.layout.fragment_chat_record;
    }

    @OnClick({R.id.card_noun, R.id.card_verb, R.id.card_adjective, R.id.card_adverb})
    public void onViewClicked(View view) {
        Fragment selectedFragment;
        switch (view.getId()) {
            case R.id.card_noun:
                selectedFragment = WordFragment.newInstance("nouns");
                transactionFragment(selectedFragment);
                break;
            case R.id.card_verb:
                selectedFragment = WordFragment.newInstance("verbs");
                transactionFragment(selectedFragment);
                break;
            case R.id.card_adjective:
                selectedFragment = WordFragment.newInstance("adjs");
                transactionFragment(selectedFragment);
                break;
            case R.id.card_adverb:
                selectedFragment = WordFragment.newInstance("adverbs");
                transactionFragment(selectedFragment);
                break;
        }
    }

    public void transactionFragment(Fragment fragment) {
//        mFragmentManager.beginTransaction().replace(R.id.checklist, fragment, Config.LIST_FRAGMENT).addToBackStack(Config.LIST_FRAGMENT).commit();

        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.LEARNING_CAT_FRAGMENT);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, Config.LEARNING_LIST_FRAGMENT);
        fragmentTransaction.show(fragment);
        fragmentTransaction.hide(curFragment);
        fragmentTransaction.addToBackStack(Config.LEARNING_CAT_FRAGMENT);
        fragmentTransaction.commit();
    }

    public void goToHistory() {
        Fragment historyFragment = HistoryFragment.newInstance();
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.LEARNING_CAT_FRAGMENT);
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, historyFragment, Config.LEARNING_HISTORY_FRAGMENT);
        fragmentTransaction.show(historyFragment);
        fragmentTransaction.hide(curFragment);
        fragmentTransaction.addToBackStack(Config.LEARNING_CAT_FRAGMENT);
        fragmentTransaction.commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void goBack() {
        chatRecordAdapter.clearModels();
        if (getActivity() instanceof MainActivity) {
            MainActivity activity = (MainActivity) getActivity();
            activity.setNavBarVisiable(false);
        }
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //Tag name is pre fragment
        Fragment preFragment = mFragmentManager.findFragmentByTag(Config.CHATTING_FRAGMENT);
        //Tag name is pre current fragment
        Fragment curFragment = mFragmentManager.findFragmentByTag(Config.CHATTING_RECORD_FRAGMENT);

        fragmentTransaction.hide(curFragment);
        fragmentTransaction.remove(curFragment);
        fragmentTransaction.show(preFragment);
        fragmentTransaction.commit();
    }



    @Override
    public boolean onBackPressed() {
        //if you don't need take care on back button, remove implements FragmentBackHandler
        goBack();
        return false;
    }
    }


