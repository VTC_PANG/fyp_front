package com.infotronic.socialchatapp.ui.ViewModel;

import androidx.lifecycle.ViewModel;

import com.infotronic.socialchatapp.roomDB.entity.Message;
import com.infotronic.socialchatapp.roomDB.entity.Users;

public class ChatRecordViewModel extends ViewModel {
    public Message message;

    public ChatRecordViewModel() {}

    public ChatRecordViewModel(Message message) {
        this.message = message;
    }

}