package com.infotronic.socialchatapp.roomDB.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.infotronic.socialchatapp.roomDB.entity.Users;


@Dao
public interface UsersDao {
    @Query("SELECT * FROM Users WHERE id = :id")
    Users getById(String id);

    @Query("SELECT * FROM Users WHERE username = :uname")
    Users getByUsername(String uname);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Users users);

    @Update
    void update(Users user);
}
