package com.infotronic.socialchatapp.roomDB.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Words {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "word")
    private String word;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "score")
    private int score;

    public Words() {
    }

    public Words(String word, String type, int score) {
        this.word = word;
        this.type = type;
        this.score = score;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Words{" +
                "word='" + word + '\'' +
                ", type='" + type + '\'' +
                ", score=" + score +
                '}';
    }

}
