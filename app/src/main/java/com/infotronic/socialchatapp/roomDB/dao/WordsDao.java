package com.infotronic.socialchatapp.roomDB.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.roomDB.entity.Words;

import java.util.List;


@Dao
public interface WordsDao {
    @Query("SELECT * FROM Words WHERE word = :word")
    Words getByName(String word);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Words> words);

    @Update
    void update(Words words);

    @Query("SELECT * FROM Words")
    LiveData<List<Words>> getAllWords();

    @Query("SELECT * FROM Words")
    List<Words> getAll();
}
