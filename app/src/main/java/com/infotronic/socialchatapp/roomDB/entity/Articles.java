package com.infotronic.socialchatapp.roomDB.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Articles {
    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "sender")
    private String sender;

    @ColumnInfo(name = "content")
    private String content;

    @ColumnInfo(name = "time")
    private String time;


    public Articles() {
    }

    public Articles(@NonNull String id, String sender, String content, String time) {
        this.id = id;
        this.sender = sender;
        this.content = content;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Articles{" +
                "id='" + id + '\'' +
                ", sender='" + sender + '\'' +
                ", content='" + content + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
