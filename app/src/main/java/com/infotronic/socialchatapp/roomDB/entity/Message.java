package com.infotronic.socialchatapp.roomDB.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Message {
    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "sender")
    private String sender;

    @ColumnInfo(name = "receiver")
    private String receiver;

    @ColumnInfo(name = "messages")
    private String messages;

    @ColumnInfo(name = "time")
    private String time;


    public Message() {
    }

    public Message(String sender, String receiver, String messages, String time) {
        this.sender = sender;
        this.receiver = receiver;
        this.messages = messages;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", messages='" + messages + '\'' +
                ", time='" + time + '\'' +
                '}';
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
