package com.infotronic.socialchatapp.roomDB;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.infotronic.socialchatapp.Config;
import com.infotronic.socialchatapp.roomDB.dao.WordsDao;
import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.roomDB.dao.UsersDao;
import com.infotronic.socialchatapp.roomDB.entity.Words;

import org.jetbrains.annotations.NotNull;

import java.io.File;

@Database(
    entities = {
        Users.class,
        Words.class
    },
    version = 1,
    exportSchema = false
)

public abstract class AppDatabase extends RoomDatabase {
    //LocalDB config
    private static String DB_NAME = "chatDB";
    private static AppDatabase appDatabase = null;

    public static AppDatabase getInstance(Context context) {
        String USER_DB_NAME;
        SharedPreferences version = context.getSharedPreferences(Config.PREF_KEY, Context.MODE_PRIVATE);

        String uid = version.getString(Config.PREF_USER_ID_KEY, "");

        String encode = version.getString(Config.PREF_Login_KEY, "");

        if (encode.equals("")) {
            if (!Config.getUid().equals("null")) {
                USER_DB_NAME = Config.getUid() + "_" + DB_NAME;
            } else {
                USER_DB_NAME = DB_NAME;
            }
        } else {
            if (!uid.equals("null")) {
                USER_DB_NAME = uid + "_" + DB_NAME;
            } else {
                if (!Config.getUid().equals("null")) {
                    USER_DB_NAME = Config.getUid() + "_" + DB_NAME;
                } else {
                    USER_DB_NAME = DB_NAME;
                }
            }
        }
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, USER_DB_NAME)
                    .build();
        }
        return appDatabase;
    }

    public static void deleteDB(@NotNull Context context) {
        File[] allDb = new File(context.getApplicationInfo().dataDir + "/databases").listFiles();
        if (allDb != null) {
            for (final File file : allDb) {
                file.delete();
            }
        }
    }

    public static void createDB(Context context) {
        appDatabase = null;
        getInstance(context);
    }

    public abstract UsersDao usersDao();
    public abstract WordsDao wordsDao();


}
