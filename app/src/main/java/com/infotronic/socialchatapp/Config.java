package com.infotronic.socialchatapp;

import java.util.ArrayList;
import java.util.List;

public class Config {
    public static String BASE_URL;

    //api
    final public static String API_TOKEN = "/authenticate";
    final public static String API_USER = "/user";
    final public static String API_SYNC = "/sync";

    //shared prefs
    final public static String PREF_KEY = "checklist";
    final public static String PREF_Login_KEY = "login-preference";
    final public static String PREF_USER_ID_KEY = "userId-preference";
    final public static String VERSION_KEY = "checklist_version";

    //Fragment
    final public static String LIST_FRAGMENT = "LIST_FRAGMENT";
    final public static String LIST_ITEM_FRAGMENT = "LIST_ITEM_FRAGMENT";
    final public static String CAT_FRAGMENT = "CAT_FRAGMENT";
    final public static String ADDRESS_FRAGMENT = "ADDRESS_FRAGMENT";
    final public static String HOME_FRAGMENT = "HOME_FRAGMENT";
    final public static String CHATTING_FRAGMENT = "CHATTING_FRAGMENT";
    final public static String CHATTING_RECORD_FRAGMENT = "CHATTING_RECORD_FRAGMENT";
    final public static String MOMENT_FRAGMENT = "MOMENT_FRAGMENT";
    final public static String ADD_MOMENT_FRAGMENT = "ADD_MOMENT_FRAGMENT";
    final public static String SETTING_FRAGMENT = "SETTING_FRAGMENT";
    final public static String DUMMY_FRAGMENT = "DUMMY_FRAGMENT";
    final public static String LEARNING_CAT_FRAGMENT = "LEARNING_CAT_FRAGMENT";
    final public static String LEARNING_LIST_FRAGMENT = "LEARNING_LIST_FRAGMENT";
    final public static String LEARNING_HISTORY_FRAGMENT = "LEARNING_HISTORY_FRAGMENT";

    public static ArrayList<Integer> getAll_address_id() {
        return all_address_id;
    }

    public static void setAll_address_id(ArrayList<Integer> all_address_id) {
        Config.all_address_id = all_address_id;
    }

    private static ArrayList<Integer> all_address_id;

    //user data
    private static String uname = "";
    private static String uid = "null";
    private static String pwd = "";

    public static String SEND_ID = "SEND_ID";
    public static String RECEIVE_ID = "RECEIVE_ID";
    public static String AI_ID = "wcp31H0y20SstfrbQdj9AIZmgVK2";

    public static String getUid() {
        return uid;
    }

    public static void setUid(String uid) {
        Config.uid = uid;
    }


    public static String getDbUser() {
        return dbUser;
    }

    public static void setDbUser(String dbUser) {
        Config.dbUser = dbUser;
    }

    private static String dbUser = "";

    //control can clickable
    public static boolean clickable = true;

    public static String getLocaleCode() {
        return localeCode;
    }

    public static void setLocaleCode(String localeCode) {
        Config.localeCode = localeCode;
    }

    private static String localeCode = "zht";

    public static String TOKEN_ACCESS = "";

    public static void flushData(){
    }

}
