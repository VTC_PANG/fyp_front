package com.infotronic.socialchatapp.service.db;


import com.infotronic.socialchatapp.roomDB.entity.Users;

public interface UsersService {
    Users getById(String id);

    Users getByUsername(String uname);

    void insertAll(Users users);

    void update(Users user);
}
