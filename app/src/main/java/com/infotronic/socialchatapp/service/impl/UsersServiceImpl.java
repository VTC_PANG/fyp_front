package com.infotronic.socialchatapp.service.impl;

import android.content.Context;
import android.os.AsyncTask;


import com.infotronic.socialchatapp.roomDB.AppDatabase;
import com.infotronic.socialchatapp.roomDB.dao.UsersDao;
import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.service.db.UsersService;

import java.util.concurrent.ExecutionException;

public class UsersServiceImpl implements UsersService {
    private UsersDao usersDao;

    public UsersServiceImpl(Context context) {
        usersDao = AppDatabase.getInstance(context).usersDao();
    }

    @Override
    public Users getById(final String id) {
        try {
            Users users = new AsyncTask<Void, Void, Users>() {
                @Override
                protected Users doInBackground(Void... voids) {
                    return usersDao.getById(id);
                }
            }.execute().get();
            return users;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Users getByUsername(String uname) {
        try {
            Users users = new AsyncTask<Void, Void, Users>() {
                @Override
                protected Users doInBackground(Void... voids) {
                    return usersDao.getByUsername(uname);
                }
            }.execute().get();
            return users;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insertAll(final Users users) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                usersDao.insertAll(users);
                return null;
            }
        }.execute();
    }

    @Override
    public void update(final Users user) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                usersDao.update(user);
                return null;
            }
        }.execute();
    }
}
