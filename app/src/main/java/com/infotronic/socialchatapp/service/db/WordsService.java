package com.infotronic.socialchatapp.service.db;


import androidx.lifecycle.LiveData;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.roomDB.entity.Words;

import java.util.List;

public interface WordsService {
    Words getByName(String word);

    void insertAll(List<Words> words);

    void update(Words words);

    LiveData<List<Words>> getAllWords();

    List<Words> getAll();

}
