package com.infotronic.socialchatapp.service.impl;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.infotronic.socialchatapp.roomDB.AppDatabase;
import com.infotronic.socialchatapp.roomDB.dao.UsersDao;
import com.infotronic.socialchatapp.roomDB.dao.WordsDao;
import com.infotronic.socialchatapp.roomDB.entity.Users;
import com.infotronic.socialchatapp.roomDB.entity.Words;
import com.infotronic.socialchatapp.service.db.UsersService;
import com.infotronic.socialchatapp.service.db.WordsService;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class WordsServiceImpl implements WordsService {
    private WordsDao wordsDao;
    private LiveData<List<Words>> allWords;
    public WordsServiceImpl(Context context) {
        wordsDao = AppDatabase.getInstance(context).wordsDao();
        allWords = wordsDao.getAllWords();
    }

    @Override
    public Words getByName(final String word) {
        try {
            Words words = new AsyncTask<Void, Void, Words>() {
                @Override
                protected Words doInBackground(Void... voids) {
                    return wordsDao.getByName(word);
                }
            }.execute().get();
            return words;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insertAll(final List<Words> words) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                wordsDao.insertAll(words);
                return null;
            }
        }.execute();
    }

    @Override
    public void update(final Words words) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                wordsDao.update(words);
                return null;
            }
        }.execute();
    }

    @Override
    public LiveData<List<Words>> getAllWords() {
        return allWords;
    }

    @Override
    public List<Words> getAll() {
        try {
            List<Words> words = new AsyncTask<Void, Void, List<Words>>() {
                @Override
                protected List<Words> doInBackground(Void... voids) {
                    return wordsDao.getAll();
                }
            }.execute().get();
            return words;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
