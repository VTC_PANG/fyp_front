package com.infotronic.socialchatapp.help.appbase;

import java.util.Random;

public class BotResponse {
    static Random random = new Random();
    private static String result = "I don't understand";

    public static String basicResponse(String msg){
        if(msg.contains("flip") && msg.contains("coin")) {
            int rand = random.nextInt(2);
            if(rand == 0) result = "heads"; else result = "tails";

            result = "I flipped a coin and it landed on " + result;
        }
        else if(msg.contains("how are you")){
            int rand = random.nextInt(3);
            switch (rand){
                case 0:
                    result = "I'm doing fine, thanks";
                    break;
                case 1:
                    result = "I'm hungry...";
                    break;
                case 2:
                    result = "Pretty good! How about you?";
                    break;
            }
        }
        else if(msg.contains("hello") || msg.contains("hi")){
            int rand = random.nextInt(3);
            switch (rand){
                case 0:
                    result = "Hello there";
                    break;
                case 1:
                    result = "Sup";
                    break;
                case 2:
                    result = "Glad to see you!";
                    break;
            }
        }
        else if(msg.contains("music") || msg.contains("song")){
            int rand = random.nextInt(4);
            if(msg.contains("classic")){
                return result = "https://www.youtube.com/watch?v=4Tr0otuiQuU&ab_channel=andrearomano";
            }
            switch (rand){
                case 0:
                    result = "https://www.youtube.com/watch?v=09R8_2nJtjg&ab_channel=Maroon5VEVO";
                    break;
                case 1:
                    result = "https://www.youtube.com/watch?v=YQHsXMglC9A&ab_channel=AdeleVEVO";
                    break;
                case 2:
                    result = "https://www.youtube.com/watch?v=zABLecsR5UE&ab_channel=LewisCapaldiVEVO";
                    break;
                case 3:
                    result = "https://www.youtube.com/watch?v=k2qgadSvNyU&ab_channel=DuaLipa";
                    break;
            }
        }
        else {
            int rand = random.nextInt(2);
            switch (rand){
                case 0:
                    result = "I don't understand";
                    break;
                case 1:
                    result = "Try asking me something different";
                    break;
            }
        }

        return result;
    }

}
