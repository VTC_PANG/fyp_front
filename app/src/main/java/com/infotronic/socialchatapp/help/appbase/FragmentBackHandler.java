package com.infotronic.socialchatapp.help.appbase;

public interface FragmentBackHandler {
    boolean onBackPressed();
}
