package com.infotronic.socialchatapp.help.appbase;

import android.util.Log;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Time {
    public static String timeStamp(){
        Long timeStamp;
        String time;
        timeStamp = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        SimpleDateFormat sdf = new SimpleDateFormat("a h:mm");
        time = sdf.format(timeStamp);
        Log.d("debug_paul", time);
        return time;
    }
}
