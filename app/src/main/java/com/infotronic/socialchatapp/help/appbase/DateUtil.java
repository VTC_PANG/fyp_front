package com.infotronic.socialchatapp.help.appbase;

import java.text.SimpleDateFormat;
import java.util.Date;

public  class DateUtil {
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");


    public static String getToday() {
        return sdf.format(new Date());
    }
}
