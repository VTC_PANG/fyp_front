package com.infotronic.socialchatapp.help.appbase;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class SolveMath {
    public static Integer solveMath(String equation){
        int result = 0;
        String newEquation = equation.replace(" ", "");
        Log.d("debug_paul", newEquation);
        if(newEquation.contains("+")){
            String[] split = newEquation.split("\\+");
             result =  Integer.parseInt(split[0]) + Integer.parseInt(split[1]);
        }
        return result;
    }
}
